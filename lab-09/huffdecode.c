#include <stdio.h>
#include "huffman.h"
#include <stdlib.h>
#include <string.h>

/***********************/
/* Aaron Montano       */
/* 12.15.2016          */
/* Seection 002        */
/***********************/

/* This program is the inverse of huffencode: here, we are going
 * to take a file that has already had the huffencode algorithm
 * performed on it and "undo" the entire process to recover the 
 * original message. The original message will be printed to the
 * standard output stream. */

/* First, we are going to declare some important arrays that will be 
 * used throughout this process. huffCodeArray, charData and
 * prioQue. */
char* huffCodeArray[256];
unsigned long charData[256];
struct HuffNode* prioQue[256];

/* An array that will keep track of how many times a character in the
 * stream has been counted. */
int countedArray[256];

/* Varibles that will be used in the readABit method: an int to keep
 * track of where we are at, a counter and an int to indicate the next
 * bit that is up. */
int counter = 0;
int nextBit = 0;
int bit;

/* Length of the priority queue. */
int prioLength = 0;

/* Huffman Tree Node declaration. */
struct HuffNode
{

  /* Frequency count. */
  unsigned long count;

  /* Symbol. */
  unsigned char sym;

  /* References to two other HuffNodes. */
  struct HuffNode* left;
  struct HuffNode* right;

};

/* Creates a HuffNode to be used. */
struct HuffNode* createNode(int k)
{

  /* Initialize another HuffNode. */
  struct HuffNode* temp = malloc(sizeof(struct HuffNode));

  /* Set the values of the temp node. */
  temp->count = charData[k];
  temp->sym = k;

  /* Initialize the pointers to the left and right node to be                                                                                                                                    
   * NULL. */
  temp->right = NULL;
  temp->left = NULL;

  /* Return the node. */
  return temp;
}

/* The very first thing that needs to be done is have the header of the input
 * file to be read and have all of the data stored within the respective arrays above. */
void readHead(FILE* inFile)
{

  /* Data notification. */
  int character;
  int numDistSym;
  unsigned long letterCount;

  /* Loop iterators. */
  int i;

  /* Fill up the charData array. */
  for(i = 0; i < 256; i ++)
  {
    charData[i] = 0;
  }

  /* First, we are going to retrieve the total number of distinct symbols in
   * the encoded file. */
  numDistSym =(int)fgetc(inFile);

  /* Next, we are going to use numDistSym to keep reading in data and assigning
   * it to the appropriate global array. */
  while(numDistSym)
  {
    
    /*character = (int)(fgetc(inFile));*/
    fread(&character, sizeof(unsigned char), 1, inFile);

    /* Next, we are going to retrieve the count for the read-in letter. */
    fread(&letterCount, sizeof(unsigned long), 1, inFile);

    /* Next, we are going to assign these values to the appropriate positions
     * in the charData array. */
    charData[character] = letterCount;

    /* Update the current numDistSym. */
    numDistSym--;
  }

  fread(&letterCount, sizeof(unsigned long), 1, inFile);

}

/* Now that we have the charData array set up, we are ready to implement it
 * and create the binary tree. */

/* A method that will fill up the prioQue array (will not be sorted yet). */
void fillUpPrio()
{

  /* Loop iterator. */
  int i;
  int j;
  j = 0;

  /* First, fill up the array will NULLs. */
  for(i = 0; i < 256; i++)
  {
    prioQue[i] = NULL;
  }
  /* Loop through the entire possible array and fill up with found characters. */
  for(i = 0; i < 256; i++)
  {

    /* If the respective position in charData has any nonzero                                                                                                                                        * frequencies in it, we will create a HuffNode representing it. */
    if(charData[i] != 0)
    {
      /* Create the HuffNode. */
      struct HuffNode *temp = createNode(i);

      /* Place it in the prioQue. */
      prioQue[j] = temp;
      j++;

      /* Update the length of prioLength. */
      prioLength++;
    }
  }
}

/* An algorithm that will be passed in the method after this that will                                                                                                                          
 * generate a priority queue of HuffNodes from the array of HuffNodes. */
int queueAlg(const void* first, const void* second)
{

  /* Create two temporary nodes that will compare each other. */
  struct HuffNode** a = (struct HuffNode**)first;
  struct HuffNode** b = (struct HuffNode**)second;

  if((*a)->count < (*b)->count) return -1;
  else if((*a)->count > (*b)->count) return 1;
  else if((*a)->sym < (*b)->sym) return 1;
  else if((*a)->sym > (*b)->sym) return -1;
  else return 0;

}

/* A method that will sort an array of HuffNode* through the use of the sorting algorithm declared above. */
void Sort()
{

  qsort(prioQue, prioLength, sizeof(struct HuffNode*), queueAlg);

}

/* The algorithm that is going to be used to construct the binary tree. Assuming that all of the data                                                                                           
 * has already been placed in a priority queue, the first two HuffNodes will be removed through the use                                                                                         
 * of the removeNode method. After that, a new tree will be created with these two removed nodes as                                                                                            
 * children (the count of this node will be the sum of the two children counts). After that, this new                                                                                           
 * tree will be added to the priority queue. This process will continue for as long as prioLength > 1. */
void createTree()
{

  struct HuffNode* first;
  struct HuffNode* second;

  int i;
  int length = prioLength;

  /* Begin while loop for as long as prioLength > 1. */
  while(length > 1)
  {

    /* Create a new node. */
    struct HuffNode* newNode = malloc(sizeof(struct HuffNode));

    Sort();

    /* Retrive the first two elements in in prioQueue. */
    first = prioQue[0];
    second = prioQue[1];

    /* Set up the newNode. */
    newNode->left = first;
    newNode->right = second;
    newNode->sym = newNode->right->sym;
    newNode->count = (newNode->left->count) + (newNode->right->count);

    /* Assign this newNode to the first position in the array. */
    prioQue[0] = newNode;

    /* Decrement prioLength. */
    prioLength--;
    length--;

    /* Now, we are going to shift all of the elements in the prioQue (past                                                                                                                     
     * the first element) to the right by one. */
    for(i = 1; i <= length; i++)
    {
      prioQue[i] = prioQue[i + 1];
    }
  }
}



/* A method that is going to read a single bit from the inFile. Will return a
 * 1 or a 0. */
int readABit(FILE* inFile)
{

  /* Check to see if we are at the beginning of the count */
  if(counter == 0)
  {
    bit = fgetc(inFile);
    counter = (1 << (8 - 1));
  }

  /* Update variables. */
  nextBit = bit/counter;
  bit %= counter;
  counter /= 2;
  
  /* Return the nextBit. */
  return nextBit;

}

/* A method that will decode a character through the implementation
 * of the readABit method defined above. */
int readChar(FILE* inFile, struct HuffNode* treeHead)
{
  
  /* Here, we are going to keep looping for as long as either of
   * the pointers to the left or right node is not NULL. */
  while(treeHead->right || treeHead->left)
  {
    /* If the read-in bit is 1, we will traverse right. */
    if(readABit(inFile)) 
    {
      treeHead = treeHead->right;
    }
    /* Otherwise, we are going to traverse to the left. */
    else
    {
      treeHead = treeHead->left;
    }

  }

  /* Once we have hit a leaf node (i.e., a HuffNode that contains the letter),
   * then we are going to return the letter that is held there. */
  return (int)treeHead->sym;
}

/* Free memory used by the tree. */
void freeTree(struct HuffNode* root)
{

  /* Create TreeNodes to hold values. */
  /* Just as in the linked list, we are going                                                                                                                                                    
   * to traverse each subtree. If a subtree                                                                                                                                                      
   * is NULL, we will not do anything to it. */
  if(root == NULL) return;

  freeTree(root->left);
  freeTree(root->right);

  free(root);


}
					    
/* The method that will perform all of the declared methods to decode a file
 * that has been Huffman encoded. */
void decodeFile(FILE* inFile, FILE* outFILE)
{
  
  /* A character that will be used to read in data. */
  int c;
  /* Loop iterator. */
  int i;

  /* The first thing that we need to do is read in the header information. */
  readHead(inFile);

  /* Fill up the priority queue. */
  fillUpPrio();

  /* Create the binary tree. */
  createTree();
  
  for(i = 0; i < 256; i++)
  {
    countedArray[i] = 0;
  }

  /* Decode the file. */
  while(!feof(inFile))
  {
    /* Read in a character. */
    c = readChar(inFile, prioQue[0]);

    /* Increment counted Array. */
    countedArray[c] = countedArray[c] + 1;

    /* Check to see if we are able to place the character in or not. */
    if((int)charData[c] >= countedArray[c]) fputc(c, outFILE);
  }

  /* Free all of the memory. */
  freeTree(prioQue[0]);

}

int main(int argc, char** argv)
{
  char* infile;
  char* outfile;
  FILE* in;
  FILE* out;

  if(argc != 3) 
  {
    printf("wrong number of args\n");
    return 1;
  }

  infile = argv[1];
  outfile = argv[2];

  in = fopen(infile, "rb");
  if(in == NULL)
  {
    printf("couldn't open %s for reading\n", infile);
    return 2;
  }

  out = fopen(outfile, "wb");
  if(out == NULL)
  {
    printf("couldn't open %s for writing\n", outfile);
    return 3;
  }

  decodeFile(in, out);

  fclose(in);
  fclose(out);

  return 0;
}
