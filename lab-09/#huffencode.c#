#include <stdio.h>
#include "huffman.h"
#include <stdlib.h>
#include <string.h>

/**************************/
/* Aaron Montano          */
/* 11.27.2016             */
/* Section 002            */
/**************************/

/* This program is going to encode a file using the Huffman algorithm. The main
 * method is going to take two command line arguments: the first will be the name
 * of the file who's contents are going to be encoded and the second will be the 
 * name of the encoded file. If the files, first and or second, cannot be opened,
 * then we will return an error. The Huffman algorithm will be described in detail
 * in its respective method declaration. */

/* Since we are dealing with the standard input stream, declare an int value to
 * hold all of the read-in characters. */
int c;

/* An array to hold all of the data regarding the characters that are read in. */
unsigned long charData[256];

/* A long string that is going to be a representation of the encoded file. */
char* binString = "";

/* An array of char* to hold all of the huffman codes for each respective character.
 * Each index of this array will represent the character itself. */
char* huffCodeArray[256];

/* Length of the priority queue. */
int prioLength = 0;

/* An integer that will hold the total number of characters. */
int totalChar = 0;

/* An array of HuffNodes that will be used in the priority queue and tree. */
struct HuffNode* prioQue[256];

int counter = 0;
int bit = 0;

/* Huffman Tree Node declaration. */
struct HuffNode 
{

  /* Frequency count. */
  unsigned long count;
  
  /* Symbol. */
  unsigned char sym; 
  
  /* References to two other HuffNodes. */
  struct HuffNode* left;
  struct HuffNode* right;

};

/* Creates a HuffNode to be used. */
struct HuffNode* createNode(int k)
{
  
  /* Initialize another HuffNode. */
  struct HuffNode* temp = malloc(sizeof(struct HuffNode));

  /* Set the values of the temp node. */
  temp->count = charData[k];
  temp->sym = k;
  
  /* Initialize the pointers to the left and right node to be
   * NULL. */
  temp->right = NULL;
  temp->left = NULL;

  /* Return the node. */
  return temp;
}

/* Prints out all of the character frequencies and stores the number of occurences
 * in the charData array. The respective index will reperesent the character itself. */
void countChars(FILE* inFile)
{
  
  /* Initialize a loop iterator variable. */
  int i;

  /* Here, we are going to set all of the values of 
   * charData to 0 so that the data can be manipulated
   * later. */
  for(i = 0; i < 256; i++)
  {
    charData[i] = 0;
  }

  /* Next, we are going to retrieve the input from the file for
   * as long as we have not hit the EOF character. */
  while((c = getc(inFile)) != EOF)
  {
    /* Update the frequency for the respective character. */
    charData[c] = charData[c] + 1;
  }

}

/* A method that will fill up the prioQue array (will not be sorted yet). */
void fillUpPrio()
{
  
  /* Loop iterator. */
  int i;
  int j;
  j = 0;

  /* First, fill up the array will NULLs. */
  for(i = 0; i < 256; i++)
  {
    prioQue[i] = NULL;
  }

  /* Loop through the entire possible array and fill up with found characters. */
  for(i = 0; i < 256; i++)
  {
    
    /* If the respective position in charData has any nonzero
     * frequencies in it, we will create a HuffNode representing it. */
    if(charData[i] != 0)
    {
      /* Create the HuffNode. */
      struct HuffNode *temp = createNode(i);
      
      /* Place it in the prioQue. */
      prioQue[j] = temp;
      j++;

      /* Update the length of prioLength. */
      prioLength++;
    }
   
  }
}

/* An algorithm that will be passed in the method after this that will
 * generate a priority queue of HuffNodes from the array of HuffNodes. */
int queueAlg(const void* first, const void* second)
{
  
  /* Create two temporary nodes that will compare each other. */
  struct HuffNode** a = (struct HuffNode**)first;
  struct HuffNode** b = (struct HuffNode**)second;

  if((*a)->count < (*b)->count) return -1;
  else if((*a)->count > (*b)->count) return 1;
  else if((*a)->sym < (*b)->sym) return 1;
  else if((*a)->sym > (*b)->sym) return -1;
  else return 0;
  
}  

/* A method that will sort an array of HuffNode* through the use of the sorting algorithm declared above. */
void Sort()
{

  qsort(prioQue, prioLength, sizeof(struct HuffNode*), queueAlg);
  
}

/* The algorithm that is going to be used to construct the binary tree. Assuming that all of the data
 * has already been placed in a priority queue, the first two HuffNodes will be removed through the use
 * of the removeNode method. After that, a new tree will be created with these two removed nodes as 
 * children (the count of this node will be the sum of the two children counts). After that, this new
 * tree will be added to the priority queue. This process will continue for as long as prioLength > 1. */
void createTree()
{

  struct HuffNode* first;
  struct HuffNode* second;

  int i;
  int length = prioLength;
  
  /* Begin while loop for as long as prioLength > 1. */
  while(length > 1)
  {

    /* Create a new node. */
    struct HuffNode* newNode = malloc(sizeof(struct HuffNode));

    Sort();

    /* Retrive the first two elements in in prioQueue. */
    first = prioQue[0];
    second = prioQue[1];
    
    /* Set up the newNode. */
    newNode->left = first;
    newNode->right = second;
    newNode->sym = newNode->right->sym;
    newNode->count = (newNode->left->count) + (newNode->right->count);

    /* Assign this newNode to the first position in the array. */
    prioQue[0] = newNode;

    /* Decrement prioLength. */
    prioLength--;
    length--;

    /* Now, we are going to shift all of the elements in the prioQue (past
     * the first element) to the right by one. */
    for(i = 1; i <= length; i++)
    {
      prioQue[i] = prioQue[i + 1];
    }
  }
}
  
/* A method that is going to create a char variable that will hold all of the 
 * Huffman code. */
char* createChar(char* prefix, char letter)
{
  char* retVal = (char *)malloc(strlen(prefix) + 2);
  sprintf(retVal, "%s%c", prefix, letter);
  return retVal;
}

/* A method that is going to concatenate two pointers to chars (i.e., strings). */
char* concatString(const char* str1, const char* str2)
{

  /* Allocate new memory to hold the new string. The +1 is for the null-terminator. */
  char* newString = malloc(strlen(str1) + strlen(str2) + 1);
  
  strcpy(newString, str1);
  strcat(newString, str2);
  
  /* Return... */
  return newString;
}

/* A method that is going to go through the tree and print out all of the 
 * values held within it (including the Huffman codes. */
void goThroughTree(struct HuffNode* header, char* prefix)
{
  
  /* First, we are going to check to see if we are currently at
   * a leaf node. If we are, then we are going to print out the node's 
   * contents. */
  if(!header->right && !header->left)
  {
    if(header->sym < 33 || header->sym > 126)
    {      
      /* Append the HuffCodeList with the prefix. */
      huffCodeArray[header->sym] = prefix;
    }
    else
    {
      /* Append huffCodeArray with the prefix in the respective 
       * location. */
      huffCodeArray[header->sym] = prefix;
    }
    /*free(prefix);*/
  }
  /* Otherwise, we are going to have to recursively call goThroughTree to
   * continue traversing the tree until we hit a leaf node. */
  else
  {
    if(header->right)
    {
      goThroughTree(header->right, createChar(prefix, '1'));
    }
    if(header->left)
    {
      goThroughTree(header->left, createChar(prefix, '0'));
    }
    /* Free up allocated memory from prefix. */
    free(prefix);
  }

 
}

/* A method that sets up all of the important arrays. */
void setUp(struct HuffNode* header, char* prefix, unsigned long chars[])
{

  /* Loop iterator. */
  int i;

  /* Set up huffCode Array. */
  for(i = 0; i < 256; i++)
  {
    huffCodeArray[i] = "2";
  }

  /* Go through list. This is also going to fill in
   * the huffCodeArray with the appropriate codes. */
  goThroughTree(header, prefix);

}

/* Free memory used by the tree. */
void freeTree(struct HuffNode* root)
{
  
  /* Create TreeNodes to hold values. */
  /* Just as in the linked list, we are going
   * to traverse each subtree. If a subtree
   * is NULL, we will not do anything to it. */
  if(root == NULL) return;
  
  freeTree(root->left);
  freeTree(root->right);

  free(root);


}    

/* Prints out the frequency tables. */
void PRINT()
{

  /* Loop iterator. */
  int i;

  /* Print out titles. */
  printf("Symbol\tFreq\tCode\n");

  /* For loop to go through all of the important arrays. */
  for(i = 0; i < 256; i++)
  {
    /* Check to see if we are dealing with a character that actually
     * occurred in the file. */
    if(charData[i] != 0)
    {
      /* Check to see if the character is within the range of printable
       * ASCII characters. */
      if(i < 33 || i > 132)
      {
	printf("=%d\t%lu\t%s\n", i, charData[i], huffCodeArray[i]);
      }
      else
      {
	printf("%c\t%lu\t%s\n", i, charData[i], huffCodeArray[i]);
      }
    }

  }
  /* Print out the total number of characters. */
  for(i = 0; i < 256; i++)
  {
    totalChar += charData[i];
  }

  printf("Total chars = %d\n", totalChar);
}

void writeToFile(const char* pointer, FILE* outFile);

/* A method that writes the header to the output file. */
void writeHeader(FILE* outFile)
{
  /*write out total number of symbols*/
  /*loop over and write out all symbols and their corresponding frequency*/
  /*after the loop write out total number of symbols*/

  /* Loop iterator. */
  int i = 0;
  /* Total number of symbols. */
  unsigned long symTotal = 0;

  /* The first thing that needs to be done is write out the total number of symbols. */
  for(i = 0; i < 256; i++)
  {
    if(charData[i] != 0) symTotal++;
  }

  /* Write the total number of symbols to the outFile. */
  fputc(symTotal, outFile);

  /* Next, we are going to loop through and place all of the 
   * characters and their respective frequencies to the file. */
  for(i = 0; i < 256; i++)
  {
    if(charData[i] != 0)
    {
      /* Write the character. */
      fputc(i, outFile);

      /* Write the frequency. */
      fwrite(&charData[i], sizeof(unsigned long), 1, outFile);
    }
  }

  /* Put the total number of characters. */
  fwrite(&totalChar, sizeof(unsigned long), 1, outFile);

}

/* A method that will write all of the encoding bits to the new file. */
void writeToFile(const char* pointer, FILE* outFile)
{

  /* Begin loop to point to strings. */
  while(*pointer)
  {

    bit = bit*2 + *pointer - '0';
    /* Update counter. */
    counter++;

    /* Check to see if we have hit a byte boundary. */
    if(counter == 8)
    {
      /* Place the bits in the outFile. */
      fputc(bit, outFile);
      /* Reset bit and counter. */
      bit = 0;
      counter = 0;
    }

    /* Update pointer. */
    pointer++;
  }

}
    
void encodeFile(FILE* in, FILE* out)
{

  /* Loop iterator. */
  int i;

  /* Allocate memory to hold strings. */
  char* prefix = (char *)calloc(1, sizeof(char));

  /* The very first thing we need to to is count the frequency of
   * each character from the in File. */
  countChars(in);

  /* Up next, we are going to take the data from the charData array
   * and create a priority queue. */
  fillUpPrio();

  /* Create the tree... */
  createTree();

  /* Set up all of the important huffman codes in the respective point
   * in their array. */
  setUp(*prioQue, prefix, charData);

  /* Print out the table depicting all of the information. */
  PRINT();
  
  /* First, rewind the file IO. */
  rewind(in);

  /* Next, we are going to write the header information to the out file. */
  writeHeader(out);

  /* Write all of the encoded data next... */
  while((c = fgetc(in)) != EOF)
  {
    writeToFile(huffCodeArray[c], out);
  }

  /* Here, we are going to check to see if we have any remaining
   * bits that were not written into the new file. */
  if(counter != 0) writeToFile("00000000", out);

  /* MEMORY FREEING. */
  /* Frees the memory allocated from createTree. */
  freeTree(*prioQue);

  /* Free all of the memory that was previously allocated for the huff
   * codes for each character. */
  for(i = 0; i < 256; i++)
  {
    if(strcmp(huffCodeArray[i], "2")) free(huffCodeArray[i]);
  }
  
}

/* Main method. */
int main(int argc, char** argv)
{
  char* infile;
  char* outfile;
  FILE* in;
  FILE* out;

  if(argc != 3) 
  {
    printf("wrong number of args\n");
    return 1;
  }

  infile = argv[1];
  outfile = argv[2];

  in = fopen(infile, "rb");
  if(in == NULL)
  {
    printf("couldn't open %s for reading\n", infile);
    return 2;
  }

  out = fopen(outfile, "wb");
  if(out == NULL)
  {
    printf("couldn't open %s for writing\n", outfile);
    return 3;
  }
  
  encodeFile(in, out);

  fclose(in);
  fclose(out);

  return 0;
}
