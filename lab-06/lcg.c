#include <stdio.h>
#include "lcg.h"

/**********************/
/* Aaron Montano      */
/* 10 / 20 / 2016     */
/* Section 002        */
/**********************/

/* Linear congruential generator. A psuedo-random number generator that will be used to 
 * generate random integers which will then be further employed when generating 
 * cryptic code. For this portion of the project, we will be implementing lcg.h
 * (which is located in the same directory as lcg.c). 
 */

/* A method that finds the prime factors of the integer in the input
 * and multiplies all of the unique ones together. Will return the ultiplied value. */
unsigned long findP(unsigned long n)
{

  /* Intiailize a temporary value to be used in the checking. */
  unsigned long tempN = n;

  /* Final number; this is the value that will be returned. */
  unsigned long finalNum = 1;

  /* Instantiate the test divisor (at 2). */
  unsigned long testDiv = 2;

  if(tempN == 0)
  {
      return 0;
  }

  /* Begin loop that will go through and determine the prime factors of
   * n. */
  while(1)
  {
    /* First, we need to check to see if testDiv squared is greater than
     * tempN. */
    if((testDiv)*(testDiv) > tempN)
    {
      /* testDiv is a prime factor and will be multiplied to finalNum iff
       * it has not already been multiplied there. We will return here as well. */
      /*if(finalNum % tempN != 0 || finalNum == 2)
	{*/
	finalNum *= tempN;
	return finalNum;
	/*}*/

    }

    /* Since we have made it this far and not returned any value for the method, we
     * are going to check to see if the remainder of tempN divided by testDiv is 
     * 0. If it is, we will continue onto some substeps. Otherwise, the testDiv
     * will be incremented. */
    if(tempN % testDiv == 0)
    {
      
      /* testDiv is a prime factor of n. Therefore, we will multiply it to
       * finalNum iff it has not already been done. */
      /* if(finalNum % testDiv != 0)
	 {*/
	finalNum *= testDiv;
	/*}*/

      /* Next, we are going to assign tempN to tempN/testDiv for as long as 
       * the remainder of the modulus is 0. So, */
      while(tempN % testDiv == 0)
      {
	tempN /= testDiv;
      }

    }
    else
    {
      /* Since the remainder of tempN % testDiv is nonzero, we will
       * increment testDiv. */
      testDiv++;
    }

  }

  return finalNum;

}

/* Here, we are going to implement the function prototypes that were
 * initialized in lcg.h, beginning with the one that sets initial values. 
 * So, */
struct LinearCongruentialGenerator makeLCG(unsigned long m, unsigned long c)
{

  /* Initialize a temporary structure; this is what the method will return. */
  struct LinearCongruentialGenerator temp;

  /* Initialize p. */
  unsigned long p = findP(m);

  /* Initialize a. */
  unsigned long a;
  
  /** Here is a special case, since we were running into that nasty error,
   ** earlier, we will return a LinearCongruentialGenerator that contains nothing
   ** but null values to mark the end of the file **/
  if(m == 0 && c == 1)
  {
    temp.m = 0;
    temp.c = 1;
    temp.a = 0;
    temp.x = 0;
    return temp;
  }

  /* Before we assign any values to the struct, we are going to do a couple 
   * of BASIC test cases. */
  if(m <= 0)
  {
    temp.m = 0;
    temp.c = 0;
    temp.a = 0;
    temp.x = 0;
    return temp;
  }
  if(c >= m)
  {
    temp.m = 0;
    temp.c = 0;
    temp.a = 0;
    temp.x = 0;
    return temp;
  }

  /* Assign p to the structure. */
  if(m % 4 == 0)
  {
    a = 1 + 2*p;
  }
  else
  {
    a = 1 + p;
  }

  /* More error checking... */
  if(a <= 0)
  {
    temp.m = 0;
    temp.c = 0;
    temp.a = 0;
    temp.x = 0;
    return temp;
  }

  /* Otherwise, we have made it this far and no errors have been recorded. 
   * Therefore, all that needs to be done is assign values to the struct
   * pointer. */
  temp.m = m;
  temp.c = c; 
  temp.x = c;
  temp.a = a;

  return temp;
}

/* Method that increments the LinearCongruentialGenerator and returns
 * the result as an unsigned long. */
unsigned long getNextRandomValue(struct LinearCongruentialGenerator* lcg)
{
  
  /* Retrieve the current value of x. */
  unsigned long retVal = lcg->x;

  unsigned long newX = ((lcg->a)*(lcg->x) + (lcg->c)) % (lcg->m);
  lcg->x = newX;
 
  return retVal;
}
