#include <stdio.h>
#include "lcg.h"

/**********************/
/* Aaron Montano      */
/* 10 / 26 / 2016     */
/* Section 002        */
/**********************/

/* Here is the .c program that will perform all of the encryption and
 * decryption for a standard input stream. Using the Linear
 * Congruential Generator that was created earlier, we will be
 * reading in bytes and decrypting/encrypting them, depending on whether
 * or not we specified a 'd' or 'e' char at the beginning of each line. 
 * This program will also be used to decrypt an encrypted novel. */

/* Decrypt and encrypt flags. */
int eFlag = 0;
int dFlag = 0;

/* Since we are dealing with a standard input stream, we are going to
 * initialize an integer value that will hold all of the c characters. */
int s;

/* This variable will be used to print out the line numbers when data
 * is encrypted or decrypted. */
int line = 1;

/* Initialize a counter for the m-value and c-value; cannot exceed 20. */
int countM = 0;
int countC = 0;

/* Set up the values that will hold m and c in the lcg. */
unsigned long m = 0;
unsigned long c = 0;

/* Whilst performing the encryption, we are going to need to check to see
 * if the character lies in the range of 'acceptable' ASCII characters
 * (i.e., 32 to 136). This method will return an int value to determine
 * whether the character read in the from the standard input stream is
 * acceptable or not. 
 * Will return 1 if it is, and 0 if it is not. */
int isValidChar(int k)
{
  /* Simple branching. */
  if(k < 32 || k > 126)
  {
    return 0;
  }
  /* Otherwise, this is invalid data. */
  else return 1;
}

struct LinearCongruentialGenerator setUpFormat(void)
{

  struct LinearCongruentialGenerator test;

  /* First, we are going to check to see if we are performing
   * an encryption or a decryption. If the first value on the
   * line is not a 'd' or an 'e', we will return a 0; denoting
   * an error. */
  s = getchar();

  if(s == '\n' || s == EOF)
  {
    countM = countC = 0;
    return makeLCG(0, 1);
  }

  if(s != 'd' && s != 'e')
  {
    countM = countC = 0;

    /* Iterate all the way to the end of the line. */
    while((s = getchar()) != '\n');
    return makeLCG(5, 10);
  }
  
  /* Check to see if we are encrypting or decrypting. */
  if(s == 'd') dFlag = 1;
  if(s == 'e') eFlag = 1;

  /* Next, we are going to calculate the value for 'm'. */
  while((s = getchar()) != EOF)
  {
    
    /* Check to make sure we are dealing with a valid value. */
    if(s >= '0' && s <= '9')
    {
      m = m*10 + (unsigned long)s - '0';
      countM++;
      /* If the counter exceeds 20, we will return an error. */

      if(countM > 20)
      {
	countM = 0;
	while((s = getchar()) != '\n');
	return makeLCG(5,10);
      }
    }
    else if(s == ',' && countM > 0)
    {
      countM = 0;
      break;
    }
    else
    {
      countM = 0;

      /* Iterate all the way to the end of the line. */
      while((s = getchar()) != '\n');

      return makeLCG(5, 10);
    }

  }
  
  /* Reset countM back to 0. */
  countM = 0;
    
  /* Lastly, we are going to calculate a value for 'c'. */
  while((s = getchar()) != EOF)
  {
      
    /* Break out of the loop if there is a ',' character. */
    if(s == ',' || s == '\n')
    {
      if(s == '\n')
      {
	putchar(s);
	countC = 0;
	break;
      }
      countC = 0;
      break;
    }

    /* Otherwise, we do the same thing as before on the 
     * m value. */
    if(s >= '0' && s <= '9')
    {
      c = c*10 + (unsigned long)s - '0';
	
      /* We counted a c value. */
      countC++;

      /* If the counter exceeds 20, we will return
       * an error. */
      if(countC > 20)
      {
        countC = 0;
        return makeLCG(5, 10);
      }

    }
    else
    {
      countC = 0;
      while((s = getchar()) != '\n');
      return makeLCG(5, 10);
    }

  }

  if(c == 0 || m == 0)
  {
    countM = countC = 0;
    while((s = getchar()) != '\n');
    return makeLCG(5,10);
  }

  test = makeLCG(m, c);
  if(test.a > test.m)
  {
    countM = countC = 0;
    while((s = getchar()) != '\n');
    return makeLCG(5, 10);
  }

  /* Reset C counter back to 0. */
  countC = 0;

  /* All that needs to be done is have 'm' and 'c' input into the 
   * lcg method and have it return x. */
  return makeLCG(m, c);

}

/* This method is going to be used to encrypt data. 
 * Will only be called under the assumption that we are 
 * encrypting (i.e., 'e' was the first character on the line)
 * and setUpFormat() has already been called. */
void encrypt(struct LinearCongruentialGenerator* n)
{

  unsigned long b;
  unsigned long rand;
  unsigned long new;

  while((s = getchar()) != EOF)
  {
    
    /* If there is a '\n' character, break out of the loop. */
    if(s == '\n')
    {
      putchar(s);
      break;
    }

    /* Convert read-in character to an unsigned long. */
    b = (unsigned long)s;

    /* Generate random value. */
    rand = getNextRandomValue(n);

    new = b ^ (rand % 128);

    /* Decide which byte to place. */
    if(new < 32)
    {
      putchar('*');
      putchar('@' + new);
    }
    else if(new == 127)
    {
      putchar('*');
      putchar('&');
    }
    else if(new == '*')
    {
      putchar('*');
      putchar('*');
    }
    else putchar(new);
    
  }

}

/* This method is going to do the opposite of what was done earlier: DECRYPT. */
void decrypt(struct LinearCongruentialGenerator* n)
{

  unsigned long b;
  unsigned long rand; 
  unsigned long new;

  while((s = getchar()) != EOF)
    {

      /* If there is a '\n' character, break out of the loop. */
      if(s == '\n')
	{
	  putchar(s);
	  break;
	}

      b = (unsigned long)s;

      /* Generate random value. */
      rand = getNextRandomValue(n);

      if(s == '*')
      {
	s = getchar();
	if(s == '*')
	{ 
	  new = '*' ^ (rand % 128);
	  if(!isValidChar(new))
	  {
	    printf("Error\n");
	    while((s = getchar()) != '\n');
	    break;
	  }
	  putchar(new);
	}
	else if(s == '&')
	{
	  new = 127 ^ (rand % 128);
	  if(!isValidChar(new))
	  {
	    printf("Error\n");
	    while((s = getchar()) != '\n');
	    break;
	  }
	  putchar(new);
	}
       	else
	{
	  new = (s - '@');
	  new = new ^ (rand % 128);
	  if(!isValidChar(new)) 
	  {
	    printf("Error\n");
	    while((s = getchar()) != '\n');
	    break;
	  }
	  putchar(new);
	}
      }
      else
      {
	new = (b ^ (rand % 128));
        putchar(new);
      }
    }

  
}

/* Main method for testing. */
int main(void)
{
  
  struct LinearCongruentialGenerator n;
  while(1)
  {

    /* If we are at the end of file, break. */
    if(s == EOF)
      {
	break;
      }
    
    /* Set up the format of the encrypt/decryption. */
    n = setUpFormat();
    
    if(n.x == 0 && n.a == 0 && n.m == 0 && n.c == 0)
    {
      printf("%*d) Error", 5, line);
      line++;
      printf("\n");
    }
    else if(n.x == 0 && n.c == 1 && n.m == 0 && n.a == 0) continue;
    else
    {
      printf("%*d) ", 5, line);
      line++;
      if(dFlag == 1) decrypt(&n);
      else encrypt(&n);
    }

    dFlag = eFlag = 0;

    m -= m;
    c -= c;
  }
  
  return 1;

}
