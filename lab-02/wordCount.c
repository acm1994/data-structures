#include <stdio.h>

/**************************************/
/* Aaron Montano                      */
/* 09 / 04 / 2016                     */
/* CS-241 Section 002                 */
/**************************************/ 

/* Define constants to be used to denote if we are inside
   or outside of a word. */
#define IN 1
#define OUT 0 

/* Main method. */
int main()
{

  /* Int to determine whether we are inside or outside of a word. */
  int STATE;

  /* Declare an int to keep track of the iteration. */
  int c;
  int prevC;

  /* Declare variables that will hold the characters and words counted in a 
   * particular line. */
  int tempC = 0;
  int tempW = 0;

  /* Variables that will be reported at the end. */
  int cFinal = 0;
  int wFinal = 0;

  /* Variables that will hold the maximum values of words and
   * characters, respectively. */
  int maxC = 0;
  int maxW = 0;
  int maxLineC = 0;
  int maxLineW = 0;
  
  /* Declare a variable that will count the total number of lines. */
  int lineCount = 1;

  STATE = OUT;
  printf("%d.", lineCount);
  /* Begin while loop that will be true as long as were are not EOF. */ 
  while ((c =  getchar()) != EOF)
    {

      /* Test to see if we need to put the line number preceding
       * the text. */
      if (prevC == '\n')
	{
	  lineCount++;
	  printf("%d.", lineCount);
	}

      /* Test to see if we are inside or outside of a word. */
      if (c == ' ' || c == '\t' || c == '\n')
	{
	  if (c == '\n')
	    {
	      printf("[%d;%d]", tempW, tempC);
	      STATE = OUT;
	      tempW = 0;
	      tempC = 0;
	    }
	  else 
	    {
	      STATE = OUT;
	    
	    } 
	}
      else if (STATE == OUT && (c != ' ' || c != '\t'))
	{
	  STATE = IN;
	  wFinal++;
	  tempW++;

	  /* Check to see if this line has the most words in it so far. */
	  if (tempW > maxW)
	    {
	      maxW = tempW;
	      maxLineW = lineCount;
	    }
	}

      /* If the character is not a newline character, then we will increment 
       * the temporary character counting variable by one. */
      if (c != '\n')
	{
	  tempC++;
	  cFinal++;

	  /* Check to see if the current line has the most words in it
	   * so far. */
	  if (tempC > maxC) 
	    {
	      maxC = tempC;
	      maxLineC = lineCount;
	    }
	}
      
      /* Put the character in its spot. */
      putchar(c);

      /* Note the previous character (or current one). */
      prevC = c;

    }

  /* Return the summary of the findings. If the final character in
   * the stream was a '\n'.*/
  if (prevC == '\n')
    {
      printf("%d lines, %d words, %d characters\n", lineCount, wFinal, cFinal);
      printf("With %d, line %d has the most characters\n", maxC, maxLineC);
      printf("With %d, line %d has the most words\n", maxW, maxLineW);
    }
  /* Otherwise... */
  else
    {
      printf("\n%d lines, %d words, %d characters\n", lineCount, wFinal, cFinal);
      printf("With %d, line %d has the most characters\n", maxC, maxLineC);
      printf("With %d, line %d has the most words", maxW, maxLineW);
    }

  return 0;
 
}

  
