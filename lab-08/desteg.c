#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*****************************/
/* Aaron Montano             */
/* 11 - 15 - 2015            */
/* Section 002               */
/*****************************/

/* This program, desteg.c, is going to take in an .bmp file
 * from the first command line argument and have the characters 
 * that are hidden within it removed. All of these said characters
 * will be hidden within the lowest two bits of pixel data. Once 
 * we have decoded a null character, then we have reached the end of
 * the message and need not continue decrypting. */

/* These four variables are going to hold values for each of the data that
 * is extracted from the bytes. After that, they are going to be added together
 * to create a single character. */
int first;
int second;
int third;
int fourth;

/* This int value is going to hold all of the four ones that were declared
 * above. It will also be implemented through the use of putchar
 * to print out a character to the standard output stream. */
int total;

/* Array used to hold the header of the .bmp file. */
unsigned char header[54];

/* Array used to hold four bytes of pixel data. */
unsigned char byteArr[4];

/* This method is going to take a byte, axe off the last two bits, and
 * return the two bits in the appropriate positions depending on the 
 * int parameter. (xx000000, 00xx0000, 0000xx00, 000000xx). */
int axeChar(unsigned char uc, int position);

int axeChar(unsigned char uc, int position)
{

  /* Depending on the position parameter, we are going to shift the
   * bits, mask off unused ones, then shift them back. */
  if(position == 1)
  {
    return ((uc << 6) & 0xFF);
  }
  else if(position == 2)
  {
    return ((uc << 6) & 0xFF) >> 2;
  }
  else if(position == 3)
  {
    return ((uc << 6) & 0xFF) >> 4;
  }
  else if(position == 4)
  {
    return ((uc << 6) & 0xFF) >> 6;
  }
  else return -1;

}

/* Main implementation. */
int main(int argc, char* argv[])
{


  /* The very first thing that needs to be done is to open the
   * file. */
  FILE* picture = fopen(argv[1], "rb");

  /* Next, we are going to discard the header of the .bmp file. All
   * 54 of these bytes will be stored in the header array to never
   * be used throughout the rest of the program. */
  fread(header, 1, 54, picture);

  /* We are now in pixel data territory. Every four bytes of data
   * will be read in and the last two bits of each byte is going to 
   * be axed off through the use of the axeChar method, and each
   * two characters will be assigned to the first, second, third and fourth
   * int variables that were declared earlier. Then, all four of these
   * int values are going to be added together to create the bit data that
   * will constitute the character to be placed to the stanrdard output
   * stream. */

  while(!feof(picture))
  {

    /* Read in the bytes and place them in byteArr. */
    fread(byteArr, 1, 4, picture);
    
    /* Implement the axeChar method to pull out the required
     * bits from the bytes. */
    first = axeChar(byteArr[0], 1);
    second = axeChar(byteArr[1], 2);
    third = axeChar(byteArr[2], 3);
    fourth = axeChar(byteArr[3], 4);
    
    /* Add all of the bits up to create a total, then place them in 
     * the standard output stream. */
    total = first + second + third + fourth;

    /* Check to see if we are decoding a NULL value. If so,
     * then break out of the loop. */
    if(total == 0) break;
    else putchar(total);
  }

  /* Close the file. */
  fclose(picture);
  
  return 0;

}

