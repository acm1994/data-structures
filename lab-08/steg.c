#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*******************/
/* Aaron Montano   */
/* 11.11.2016      */
/* Section 002     */
/*******************/

/* This program is going to take two command line arguments: the 
 * first is going to be the image file and the latter is going 
 * to be the name of the generated file. Output file header will
 * be exactly like the input file header due to the fact that the
 * size of the image is the same. Every character of the secret
 * message will be contained in the lowest two bits of four consecutive
 * bytes of pixel data. After the last character from the entire message,
 * a NULL character will be encoded into the picture. If for any
 * reason that the message has been processed efore the end of the 
 * pixel data has been reached, the remaining bytes will be copied into
 * the output file unchanged. */

/* These two variables are going to be used when chopping off the 
 * last two bits of a character, then inserting them into a byte. 
 * chopByte will be the by that is chopped off.
 * lastTwo will be the byte that has the last two bits from the
 * input character. */
unsigned char chopByte;
unsigned char lastTwo;

/* Since we are dealing with the standard input stream, we need to 
 * use an int value to hold all of the characters. */
int c;

/* Method prototype for a method that will take in a byte and 
 * shut off the first two bits depending on what these first
 * two are. */
unsigned char shutOffTwo(unsigned char s);

/* Implementation for the shutOffTwo method. */
unsigned char shutOffTwo(unsigned char s)
{
 
  /* Shift the bits two to the right, then shift them 
   * back to mask off the last two bits. */
  return (s >> 2) << 2;

}

/* Method prototype for a method that is going to axe off
 * two bits from a read-in character, depending on which
 * "position" it is in. */
unsigned char axeChar(unsigned char s, int a);

/* axeChar implementation. */
unsigned char axeChar(unsigned char s, int a)
{
  /* Depending on which quadrant (specified by a),
   * we will shift off some bits to get the 
   * ones requied. */
  if(a == 4)
  {
    /* Shift to the left 6 bits then mask off the 
     * remaining 8. */
    return ((s << 6) & 0xFF) >> 6;
  }
  else if(a == 3)
  {
    return (((s >> 2) << 6) & 0xFF) >> 6;
  }
  else if(a == 2)
  {
    return (((s >> 4) << 6) & 0xFF) >> 6;
  }
  else if(a == 1)
  {
    return (((s >> 6) << 6) & 0xFF) >> 6;
  }
  else return -1;

}

/* Main method. */
int main(int argc, char* argv[])
{

  /* For loop variable. */
  int i;

  /* Initialize an array to hold the header of the
   * file. After this has been written to the new file, it will
   * never be used again. */
  unsigned char header[54];

  /* Initialize an array to hold the bytes of the file (this will
   * be implemented after the header has been written to the new file. */
  unsigned char byteArr[4];
   
  /* Open the original file. */
  FILE* origFile = fopen(argv[1], "rb");

  /* Instantiate file to be written to .*/
  FILE* newFile = fopen(argv[2], "wb");
 
  /* The very first thing that needs to be done is have the 
   * header of the original file copied to the new file unchanged. */
  fread(header, 1, sizeof(header), origFile); 
  fwrite(header, 1, sizeof(header), newFile);

  /* Use while loop to go through the entire file for as long
   * as we have not hit the end. */
  while(!feof(origFile))
  {

    c = getchar();

    if(c == EOF) 
    {
      fread(byteArr, 1, sizeof(byteArr), origFile);
      byteArr[0] = shutOffTwo(byteArr[0]);
      byteArr[1] = shutOffTwo(byteArr[1]);
      byteArr[2] = shutOffTwo(byteArr[2]);
      byteArr[3] = shutOffTwo(byteArr[3]);
      fwrite(byteArr, 1, sizeof(byteArr), newFile);
      break;
    }

    /* Read the original file for four bytes of raw data. */
    fread(byteArr, 1, sizeof(byteArr), origFile);

    for(i = 4; i >= 1; i--)
    {
      /* Shut off the last two bits of extracted bytes. */
      chopByte = shutOffTwo(byteArr[i-1]);

      /* Take the read-in character and axe off the appropriate 
       * bits. */
      lastTwo = axeChar(c, i);

      /* Reassign the byte. */
      byteArr[i-1] = chopByte + lastTwo;
    }

    /* Now, we are going to write the byteArr to the newFile. */
    fwrite(byteArr, 1, sizeof(byteArr), newFile);
    
  }

  /* If there is data that has not been written into the new
   * file, then continue writing it in. */
  while((c = fgetc(origFile)) != EOF)
  {
    fputc(c, newFile);
  }

  /* Close the files. */
  fclose(origFile);
  fclose(newFile);

  return 0;
  
}
