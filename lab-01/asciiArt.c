x/******************************/
/* Aaron Montano              */
/* 08 / 25 / 2016             */
/* Section 002                */
/******************************/

#include <stdio.h>

/* Main method. */
int main(void)
{
  
  printf("\n");
  printf("\n");
  printf("               AAA                                                                                          \n");
  printf("              A:::A                                                                                         \n");
  printf("             A:::::A                                                                                        \n");
  printf("            A:::::::A                                                                                       \n");
  printf("           A:::::::::A           aaaaaaaaaaaaa  rrrrr   rrrrrrrrr      ooooooooooo   nnnn  nnnnnnnn         \n");
  printf("          A:::::A:::::A          a::::::::::::a r::::rrr:::::::::r   oo:::::::::::oo n:::nn::::::::nn       \n");
  printf("         A:::::A A:::::A         aaaaaaaaa:::::ar:::::::::::::::::r o:::::::::::::::on::::::::::::::nn      \n");
  printf("        A:::::A   A:::::A                 a::::arr::::::rrrrr::::::ro:::::ooooo:::::onn:::::::::::::::n     \n");
  printf("       A:::::A     A:::::A         aaaaaaa:::::a r:::::r     r:::::ro::::o     o::::o  n:::::nnnn:::::n     \n");
  printf("      A:::::AAAAAAAAA:::::A      aa::::::::::::a r:::::r     rrrrrrro::::o     o::::o  n::::n    n::::n     \n");
  printf("     A:::::::::::::::::::::A    a::::aaaa::::::a r:::::r            o::::o     o::::o  n::::n    n::::n     \n");
  printf("    A:::::AAAAAAAAAAAAA:::::A  a::::a    a:::::a r:::::r            o::::o     o::::o  n::::n    n::::n     \n");
  printf("   A:::::A             A:::::A a::::a    a:::::a r:::::r            o:::::ooooo:::::o  n::::n    n::::n     \n");
  printf("  A:::::A               A:::::Aa:::::aaaa::::::a r:::::r            o:::::::::::::::o  n::::n    n::::n     \n");
  printf(" A:::::A                 A:::::Aa::::::::::aa:::ar:::::r             oo:::::::::::oo   n::::n    n::::n     \n");
  printf("AAAAAAA                   AAAAAAAaaaaaaaaaa  aaaarrrrrrr               ooooooooooo     nnnnnn    nnnnnn     \n");
  printf("\n");
  printf("\n");

  // Return statement.
  return 0;

}
