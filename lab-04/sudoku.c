#include <stdio.h>

/*********************/
/* Aaron Montano     */
/* 09/20/2016        */
/* Section 002       */
/*********************/

/* Since we are implementing the standard input stream for this program,
 * let c be the variable that holds these values. */  
int c;

/* A value that will be used to count the number of standard input
 * characters. */
int count = 0;
 
/* Values to set up the board. */
int i = 0;
int j = 0;

/* Initialize a 9x9 character array to hold all of the values. */
char array[9][9];

/* To fix the spacing, we are going to set up a flag to see if it started or not. */
int started = 0;

int safe(char board[9][9], int r, int c, char num);
int checkColumn(char board[9][9], int col, char num);
int checkRow(char board[9][9], int row, char num);

/* The first thing we need to do when solving these puzzles is to make
 * sure that they are even valid in the first place. Therefore, we are
 * going to write a method that will print out the input (to match the 
 * output on the website) and check for two error cases: Are there only
 * numbers in the input stream (i.e., no letters)? Is the number of 
 * characters read in less than or more than 81 (the total number of 
 * digits in a Sudoku puzzle)? Will return a 1 if there are no errors
 (i.e., a valid puzzle) and a 0 if there exists an error within. */
int validPuzzle()
{
  int retVal = 1;
  for(i = 0; i < 9; i++)
    {
      for(j = 0; j < 9; j++)
	   {
     		array[i][j] = '.';
	   }
    }
  
  i = j = 0;

	/* Check the entire line. */
	while((c = getchar()))
	{ 
	  if(c == EOF)
	    {
	      return 2;
	    }
	  started = 1;
		/* Put c on the line. */
		putchar(c);

		if (c == '\n') break;
		
		/* Update the counter. */
		count++;
		
		/* Check to see if it is a valid character. */
		if((c < '0' || c > '9') && c != '.')
		{
		  retVal = 0;
		}
		
		/* Check to see if the puzzle is a valid length. */
		/* Will add characters to the 1x81 array for as long as 
		 * there is a valid number of characters. */
		if(count <= 81)
		{

		  /* Next, we are going to check to see if it is safe
		   * to place the current number in the permanent array. */
		  if(c != '.')
		    {
		      if(!safe(array, i, j, c)) retVal = 0;
		    }
		  
		  /* Place the number on the permanent array. */
		  array[i][j] = c;
			
		  /* Increment j++. If j == 10, then i++ and j = 0. */
		  j++;
		  if(j == 9)
		  {
		    i++;
		    j = 0;
		  }				
		}
		else
		{
			retVal = 0;
		}

	}
	
	/* Check to make sure that we counted 81 characters. */
	if (count != 81) retVal = 0;

	/* Reset i, j and count for the next puzzle. */
	i = j = count = 0;
	
	return retVal;
	
}

/* Check for an empty space on the board; return it's row number. */
int findEmptyRow(char board[9][9])
{
	int i, j;
	for(i = 0; i < 9; i++)
	{
		for(j = 0; j < 9; j++)
		{
			if(board[i][j] == '.') return i;
		}
	}
	
	return 10;
}

/* Check for an empty space on the board; return it's column number. */
int findEmptyColumn(char board[9][9])
{
	int i, j;
	for(i = 0; i < 9; i++)
	{
		for(j = 0; j < 9; j++)
		{
		  if(board[i][j] == '.')
		    {
		      return j;
		    }
		}
	}
	
	return 10;
}

/* This method will check the current column to see if a certain
 * character lies in it. Will return a 1 if this column is not
 * safe, and a 0 if it is. */
int checkColumn(char board[9][9], int curCol, char num)
{
  
  int retVal = 1;
  int i;

  for(i = 0; i < 9; i++)
    {
      if(board[i][curCol] == num) retVal = 0;
    }
  
  return retVal;
}

/* This method is going to check to see if the current row is a safe space. 
 * Will return a 1 if it is not, a 0 if it is. */
int checkRow(char board[9][9], int curRow, char num)
{
  int retVal = 1;
  int j;

  for(j = 0; j < 9; j++)
    {
      if(board[curRow][j] == num) retVal = 0;
    }

  return retVal;
}

/* This method is going to check to see if the current BOX is a safe space.
 * Will return a 1 if it is not, a 0 if it is. */
int checkBox(char board[9][9], int begRow, int begCol, char num)
{

  int retVal = 1;
  int i;
  int j;
  
  for(i = 0; i < 3; i++)
    {
      for(j = 0; j < 3; j++)
	{
	  if (board[begRow + i][begCol + j] == num) retVal = 0;
	}
    }

  return retVal;
}

/* For the sake of coding space, this method will check to make sure
 * that the puzzle is valid and or it is safe to assign a certain number 
 * there. So, */
int safe(char board[9][9], int r, int c, char num)
{
  return checkColumn(board, c, num) && checkRow(board, r, num) && checkBox(board, r - r%3, c - c%3, num);
}

/* Now, we are going to create a method that will recursively solve the sudoku puzzle. */
int solvePuzzle(char board[9][9])
{
  
  /* First, we need to find an empty spot on the board. */
  int c = findEmptyColumn(board);
  int r = findEmptyRow(board);
  char num;

  /* Next, check to see if the array is full. If so,
   * return a 1. */
  if(c == 10 && r == 10)
    {
      return 1;
    }
  
  for(num = '1'; num <= '9'; num++)
  {
      
    /* Check to see if the position is safe. */
    if(safe(board, r, c, num))
      {

	board[r][c] = num;

	if(solvePuzzle(board))
	  {
	    return 1;
	  }

	board[r][c] = '.';
      }
      }     
    return 0;
  
}

/* This method is going to take the 2-d character parameter
 * and print it out. */
void printArray(char board[9][9])
{
	int i, j;
	for(i = 0; i < 9; i++)
	{
		for(j = 0; j < 9; j++)
		{
			printf("%c", board[i][j]);
		}
	}
}

/* Main method. */
int main(void)
{

  int t = 1;
  int check;
	
  /* Begin the standard input stream. */
  while(t)
  {
    if(started) printf("\n\n");
    check = validPuzzle();
    if(check == 0)
	  {
	    printf("Error");
	  }
    else if(check == 2)
      {
	break;
      }
	  else
	  {
	    if(solvePuzzle(array))
	    {
	      printArray(array);
	    }
	    else 
	      {
		printf("No solution");
	      }
	  }
  }
  return 1;	
}
