#include <stdio.h>
#include <limits.h>

/********************************/
/* Aaron Montano                */
/* 09 / 13 / 2016               */
/* Section 002                  */
/********************************/

#define MAX_BITS 4294967295

/* Declaration of important values. */
int p, n;
int c;
unsigned x;
int complete = 0;

/* This int will indicate if an input value is too big for the
 * respective machine. */
int tooLong = 0;

/* Counter to keep track of the index of the variable 
 * recorded. */
int count = 1;

/* getbits prototype. */
unsigned getbits(unsigned x, int p, int n);

/* retInt prototype. */
unsigned int retInt();

/* Main method. */
int main(void)
{
  /* For as long as we are not complete with the text file from the
   * input stream (i.e., c != EOF), we are going to continue looping 
   * through the text input. So, */
  while(!complete)
    {
      
      /* We are going to assign the values retrieved by retInt() to either
       * x, p or n. To figure out which ones to assign it to, we are going
       * implement the counter int value. If the counter is at 1, we are going
       * to assign the value to x. If it is at 2, we are going to assign 
       * it to p. And if it is 3, we are going to assign it to n and return the
       * counter to 1. If the counter has been returned to 1, we are going to
       * compute the calculation using the getbits() method and print out
       * what is being done internally; just how it is depicted on the 
       * website. So, */
      if(count == 1)
	{ 
	  x = retInt();
	  count++;
	}
      else if(count == 2)
	{ 
	  p = (int)retInt();
	  count++;
	}
      else
	{
	  n = (int)retInt();
	  /* Return count to 1. */
	  count = 1;
	}

      /* Print out the value if the counter has been returned to 1.
       * Here is where the error testing will take place. */
      if(count == 1)
	{
	/* If we are required to shift the bits in x by a negative number. */
	if(n > (p+1))
	  {
	    printf("Error: too many bits requested from position\n");
	  }
        else if(p >= 32)
	  {
	    printf("Error: position out of range\n");
	  }
        else if(n >= 32)
	  {
	    printf("Error: number of bits out of range\n");
	  }
        else if(tooLong)
	  {
	    printf("Error: value out of range\n");
	    tooLong = 0;
	  }
      /* Otherwise... */
        else
	{
	  printf("getbits(x=%u, p=%u, n=%u) = %u\n", x, p, n, getbits(x, p, n));
	}
	}
    
    }

  return 0;

}

/* getbits function declaration. */
unsigned getbits(unsigned x, int p, int n)
{
  
	return (x >> (p+1-n)) & ~(~0 << n);
	
}

/* retInt function declaration. */
unsigned int retInt()
{
	
	/* Define an arbitrary long. */
	long number = 0;
	
	/* Retreive a character from the standard input stream. */
	c = getchar();

	while(c != '\n' && c != ',' && c != EOF)
	{

		/* Make sure that the character is in the 
		 * permissible range of values. (i.e., '0'
		 * and '9'. */
		if(c >= '0' && c <= '9')
		{
			number = number*10 + c - '0';
			/* Check to see if the number is below the maximum
			 * value of ints for the respective machine. */
			if (number > MAX_BITS)
			  {
			    tooLong = 1;
			  }
		}
	
	/* Retrieve the next character from the standard input
	 * stream. */
	c = getchar();
	}

	/* Check to see if we are at the end of the file (i.e.,
	 * c == EOF). */
	if(c == EOF)
	  {
	    complete = 1;
	  }

	/* Once the while loop is complete (i.e., we are out of the
	 * complete number), return the long number as an unsigned
	 * int. */
	return (unsigned int)number;		
			
}
