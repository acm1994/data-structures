#include <stdio.h>
#include <stdlib.h>
#include "linkedlist.h"

/******************************/
/* Aaron Montano              */
/* 11 / 02 / 2016             */
/* Section 002                */
/******************************/

/* This program is going to implement all of the function prototypes
 * from linkedlist.h, which in turn will be tested using listtest.c.
 * All of the unimplemented methods will be copied and pasted into
 * this program to have their inner details worked out. */

/* Alloc a new node with given data. */
struct ListNode* createNode(int data)
{

  /* Instantiate a ListNode struct using malloc to be
   * aware of arbitrary space. */
  struct ListNode* node = malloc(sizeof(struct ListNode));

  node->data = data;
  node->next = NULL;  
 
  /* Return the struct. */
  return node;

}

/* Insert data at appropriate place in a sorted list, return new list head. */
struct ListNode* insertSorted(struct ListNode* head, int data)
{

  /* Create newNode to be inserted. */
  struct ListNode* newNode = createNode(data);
  
  /* Temporary nodes. */
  struct ListNode* nextNode = NULL;
  struct ListNode* prevNode = NULL;
  struct ListNode* currNode;

  /* If the given head is empty, return the newNode. */
  if(head == NULL)
  {
    return newNode;
  }

  /* Otherwise, we are going to have to place this at the end of the linked list. */
  else
  {
    
    /* Loop through the list. */
    for(currNode = head; currNode != NULL; currNode = currNode->next)
    {
      nextNode = currNode->next;
      
      /* Once we have found the end of the list, place the newNode at the end. */
      if(nextNode == NULL && prevNode == NULL)
      {
	if(newNode->data > currNode->data)
	{
	  currNode->next = newNode;
	  head = currNode;
	  break;
	}
	else
	{
	  newNode->next = currNode;
	  head = newNode;
	  break;
	}

      }
      else if(nextNode == NULL && prevNode != NULL)
      {
	if(newNode->data <= currNode->data)
	{
	  prevNode->next = newNode;
	  newNode->next = currNode;
	  currNode->next = NULL;
	  break;
	}
	else
	{
	  currNode->next = newNode;
	  break;
	}
      }
      else if(prevNode == NULL && nextNode != NULL)
      {
	if(newNode->data <= currNode->data)
	{
	  newNode->next = currNode;
	  head = newNode;
	  break;
	}
	else if(newNode->data > currNode->data && newNode->data <= nextNode->data)
	{
	  currNode->next = newNode;
	  newNode->next = nextNode;
	  break;
	}
      }
      else if(prevNode != NULL && nextNode != NULL)
      {
	if(newNode->data <= currNode->data && newNode->data > prevNode->data)
	{
	  prevNode->next = newNode;
	  newNode->next = currNode;
	  break;
	}
	else if(newNode->data > currNode->data && newNode->data <= nextNode->data)
	{
	  currNode->next = newNode;
	  newNode->next = nextNode;
	  break;
	}

      }
    
      prevNode = currNode;
    }
  }
  
  return head;
}
/* Remove data from list pointed to by headRef, changing head if necessary.
 * Make no assumptions as to whether the list is sorted.
 * Memory for removed node should be freed.
 * Return 1 if data was present, 0 if not found. */
int removeItem(struct ListNode** headRef, int data)
{

  /* Temporary current node, previous node and next node. */
  struct ListNode* currNode = (*headRef);
  struct ListNode* prevNode = NULL;
  struct ListNode* nextNode = (*headRef)->next;

  while(currNode != NULL)
  {
    /* Remove at the beginning of the list. */
    if(currNode->data == data && prevNode == NULL)
    {

      /* Set head to the next pointed-to node. */
      (*headRef) = (*headRef)->next;
      currNode->next = NULL;

      /*Free the current one */
      free(currNode);

      /* Return a "YES". */
      return 1;

    }
    
    /* Remove at the end of the list. */
    if(currNode->data == data && nextNode == NULL)
    {

      /* Free the current node. */
      free(currNode);

      /* Return a "YES". */
      return 1;

    }

    /* Otherwise, we are going to have to iterate through the linked list
     * and find the data we are looking for. */
    if(currNode->data == data)
    {

      /* The previous node will now be pointing to the next node. */
      prevNode->next = nextNode;
      
      /* Free the current Node. */
      free(currNode);
      
      /* Return a YES. */
      return 1;

    }

    /* Since we did not return anything and have made it this far, we are going 
     * to update the pointers. */
    prevNode = currNode;
    currNode = nextNode;
    
    /* If the next node is a NULL, we will break out of the loop. */
    if(nextNode == NULL) break;
    nextNode = nextNode->next;
    
  }
  /* Since we have made it this far, we have not found the data and will return a 0. */
 
  return 0;

}

/* Insert data at head of list, return new list head. */
struct ListNode* pushStack(struct ListNode* head, int data)
{

  /* Create temporary ListNode. */
  struct ListNode* newNode = createNode(data);
  
  /* The 'next' struct pointer will point to the head. */
  newNode->next = head;
  
  /* The head of the list will be the newNode. */
  head = newNode;

  /* Retun the newNode. */
  return head;

}

/* Remove and return data from head of non-empty list, changing head.
 * Memory for removed node should be freed. */
int popStack(struct ListNode** headRef)
{
  
  /* Value to hold the data. */
  int retVal;

  /* ListNode to hold a temporary value. */
  struct ListNode* temp = (*headRef);

  /* Retrieve data from the head of list. */
  retVal = (*headRef)->data;
  
  /* Rewrite the head to the next value. */
  (*headRef) = (*headRef)->next;
  /* Free the data. */
  free(temp);
  
  
  

  return retVal;

}

/* Return length of the list. */
int listLength(struct ListNode* head)
{

  struct ListNode* current = head;
  /* Counter. */
  int count = 0;
  
  /* Iterate through the linked list until we hit a NULL. */
  while (current != NULL)
  {
    count++;
    current = current->next;
  }
  
  /* Return the counted value. */
  return count;

}

/* Print list data on single line, separated with spaces and ending
 * with newline. */
void printList(struct ListNode* head)
{

  /* Create temporary list. */
  struct ListNode* current = head;
  
  /* Iterate through the linked list until we hit a NULL. */
  while(current != NULL)
  {
    printf("%d ", current->data);
    
    /* Update. */
    current = current->next;
  }

  /* Newline. */
  printf("\n");

}

/* Free memory used by the list. */
void freeList(struct ListNode* head)
{
  /* Simply free the head. */
  struct ListNode* currNode = head;
  struct ListNode* tempNode;

  while(currNode != NULL)
  {
    tempNode = currNode;
    currNode = currNode->next;
    free(tempNode);
    if(currNode == NULL) break;
  }

}

/* Reverse order of elements in the list */
void reverseList(struct ListNode** headRef)
{

  /* Initialize some ListNodes to be used. */
  struct ListNode* prevNode = NULL;
  struct ListNode* currNode = *headRef;
  struct ListNode* nextNode;

  /* Iterate through the list. */
  while(currNode != NULL)
  {
    nextNode = currNode->next;
    currNode->next = prevNode;
    prevNode = currNode;
    currNode = nextNode;
  }

  *headRef = prevNode;
}
  

