#include <stdio.h>
#include <stdlib.h>
#include "binarytree.h"

/*******************/
/* Aaron Montano   */
/* 11.11.2016      */
/* Section 002     */
/*******************/


/* This program is going to implement all of the method
 * prototypes that are contained within the binarytree.h 
 * file. The methods contained in said file are going to be
 * copied and pasted into this area to have their inner
 * workings set up. */

/* Declaration of help function minValue: works just like
 * maxValueBST except that we find the minimum value in a
 0;136;0c* BST. */
int minValue(struct TreeNode* root);

/* Declaration of helper function that will be used to print
 * the newline at the end of the method that will print out
 * the leaves. */
void printHelp(struct TreeNode* root);

/* Declaration of helper function minValueNode that finds the 
 * node which contain the least value in the entire tree. This will
 * be used when removing a node containing a particular set of data. */
struct TreeNode* minValNode(struct TreeNode* root)
{
  
  /* Initialize a node. */
  struct TreeNode* current = root;
  
  /* Loop through the left side of the tree since we 
   * are dealing with the least value. */
  while(current != NULL)
  {
    current = current->left;
  }
  
  /* Once we have hit the bottom of the tree, we will
   * return the tree that contains the node. */
  return current;
}

/* Alloc a new node with given data. */
struct TreeNode* createNode(int data)
{
  
  /* Initialize empty space for a TreeNode struct. */
  struct TreeNode* node = malloc(sizeof(struct TreeNode));
  
  /* Assign the data to the node. */
  node->data = data;
  
  /* Assign the pointers to the left/right leaves as NULL. */
  node->left = NULL;
  node->right = NULL;

  /* Return the node. */
  return node;
}

/* Insert data at appropriate place in BST, return new tree root. */
struct TreeNode* insertBST(struct TreeNode* root, int data)
{
  /* A binary search tree contains the following properties:
   * the left leaf of is less than the root and the right leaf
   * is greater than the root. */
  if(root == NULL) return createNode(data);
  
  /* We are going to check to see whether the data from the 
   * method parameter is less than or greater than that
   * of the root. So, */
  if(data < root->data)
  {
    /* Since the left pointer of the node may be null,
     * we are going to recursively call insertBST. */
    root->left = insertBST(root->left, data);
  }
  else
  {
    /* Same recursive call. */
    root->right = insertBST(root->right, data);
  }

  /* Return the root. */
  return root;

}

/* Remove data from BST pointed to by rootRef, changing root if necessary.
 * For simplicity's sake, always choose node's in-order
 *   predecessor in the two-child case.
 * Memory for removed node should be freed.
 * Return 1 if data was present, 0 if not found. */
int removeBST(struct TreeNode** rootRef, int data)
{
  /* This node is going to be used when performing a 
   * two-child removal case. */
  struct TreeNode* delNode;

  /* First, check to see if the current node is NULL. So, */
  if(*rootRef == NULL) return 0;
  
  /* Otherwise, we are going to have to go through a series of tests
   * and perform the correct action. */
  
  /* First, let us check to see whether the data passed in the
   * method parameter is less than or greater than the current
   * node. If it is less than the data in the current node, we will
   * traverse down the left subtree. We will go the other way
   * if the data in the parameter is greater than that of the current
   * node. */
  else if(data < (*rootRef)->data)
  {
    return removeBST(&((*rootRef)->left), data);
  }
  else if(data > (*rootRef)->data)
  {
    return removeBST(&((*rootRef)->right), data);
  }

  /* Since we have made it this far, this else brach implies
   * that we have found the node that contains the same data as that
   * of the function. */
  else
  {
    /* If the node to be deleted contains one 
     * or no children... */
    if((*rootRef)->right == NULL)
    {
      delNode = (*rootRef);
      (*rootRef) = (*rootRef)->left;
      free(delNode);
    }
    else if((*rootRef)->left == NULL)
    {
      delNode = (*rootRef);
      (*rootRef) = (*rootRef)->right;
      free(delNode);
    }
    /* Otherwise, we have hit a case where the
     * node to be deleted contains two children. So, */
    else
    {
      (*rootRef)->data = maxValueBST((*rootRef)->left);
      return removeBST(&((*rootRef)->left), (*rootRef)->data);
    }
    return 1;
  }

}


/* Return maximum value in non-empty binary search tree. */
int maxValueBST(struct TreeNode* root)
{
  
  /* Initialize a TreeNode. */
  struct TreeNode* current = root;

  /* Since all of the values in the right subtree are going
   * to be greater than that of the one in its root,
   * we only need to recursively traverse the right subtrees
   * until we hit a null, then return the value at that last subtree. */
  while(current->right != NULL)
  {
    current = current->right;
  }

  /* Return the data at the last node. */
  return current->data;
}

/* Return maximum depth of tree. Empty tree has depth 0. */
int maxDepth(struct TreeNode* root)
{

  /* Declare two int variables. */ 
  int leftDepth;
  int rightDepth;

  /* If the root of the tree is NULL, then return 0. */
  if(root == NULL) return 0;
  
  /* Otherwise, we are going to have to recursively
   * call this method onto the left ands right subnodes. So, */
  else 
  {
    leftDepth = maxDepth(root->left);
    rightDepth = maxDepth(root->right);

    /* Now, we are going to compare leftDepth and rightDepth
     * and return the larger one. */
    if(leftDepth > rightDepth) return (leftDepth + 1);
    else return (rightDepth + 1);

    /* The '+ 1' was included because the current node was
     * not included in the computation. */
  }
}

/* A tree is balanced if both subtrees are balanced and
 * the difference in height between the subtrees is no more than 1.
 * Return 1 if tree is balanced, 0 if not. */
int isBalanced(struct TreeNode* root)
{
  
  int leftDepth;
  int rightDepth;

  /* If the given root in the function parameter is NULL, then
   * we will return a 1. */
  if(root == NULL) return 1;
  /* Otherwise, we are going to have to recursively call
   * isBalanced by implementing maxDepth. */
  /* First, we need to get the depths of both the 
   * left and right subtrees, so. */
  leftDepth = maxDepth(root->left);
  rightDepth = maxDepth(root->right);
  
  /* Next, we are going to check to see if the depths of
   * the roots differ by 1 and recursively call isBalance
   * to make sure the rest of the subtrees are balanced. */
  if(abs(leftDepth - rightDepth) <= 1 &&
     isBalanced(root->left) && isBalanced(root->right)) return 1;
  
  /* Otherwise, we have made it this far and can conclude that the tree
   * is not balanced. */
  return 0;
}

/* Return 1 if tree is a binary search tree, 0 if not. */
int isBST(struct TreeNode* root)
{
  /* If the given node is empty, return a 1. */
  if(root == NULL) return 1;
  /* Check to see if the right pointer data is less than
   * its root. Will use minvalue and maxValueBST to aid in
   * determining validity. */
  if(root->left != NULL && maxValueBST(root->left) > root->data)
  {
    return 0;
  }
  if(root->right != NULL && minValue(root->right) < root->data)
  {
    return 0;
  }

  /* Recursively check if both sides are valid. */
  if(isBST(root->left) == 0 || isBST(root->right) == 0) return 0;

  /* Otherwise, we have made it this far and can conclude
   * that it is valid. */
  return 1;
}

/* Print data for inorder tree traversal on single line,
 * separated with spaces, ending with newline. */
void printTree(struct TreeNode* root)
{
  
  /* For this method, we are going to implement a while loop. */
  struct TreeNode* previous;
  struct TreeNode* current;

  /* First, check to see if the root is NULL. If it is, then
   * return and insert newline. */
  if(root == NULL) 
  {
    printf("\n");
    return;
  }

  /* Otherwise... */
  
  /* Begin while loop... */
  current = root;
  while(current != NULL)
  {
    /* If the left pointer of the current node is NULL, or 
     * if we have hit the lefter-most portion, we will print
     * out its data. So, */
    if(current->left == NULL) 
    {
      printf("%d ", current->data);
      /* Update new value... */
      current = current->right;
    }
    /* Otherwise, keep iterating through. */
    else
    {
      /* Find previous inorder node. */
      previous = current->left;
      while(previous->right != NULL && previous->right != current)
      {
	previous = previous->right;
      }

      /* Next, we are going to make the current node a previous node inorder. */
      if(previous->right == NULL)
      {
	previous->right = current;
	current = current->left;
      }

      else
      {
	previous->right = NULL;
	printf("%d ", current->data);
	current = current->right;
      }
    }
  } 

  /* Print the newline. */
  printf("\n");
 
}

/* Does recursive action with printLeaves. Will
 * be used in the actual printLeaves method, but 
 * will then have a newline printed after it. */
void printHelp(struct TreeNode* root)
{

  /* In a binary search tree, a "leaf" is a node that
   * contains no children. */
  if(root != NULL) 
  {
    printHelp(root->left);
    printHelp(root->right);
    if(root->left == NULL && root->right == NULL)
    {
      printf("%d ", root->data);
    }
  }

}


/* Print data for leaves on single line,
 * separated with spaces, ending with newline.*/
void printLeaves(struct TreeNode* root)
{
  
  /* Call printHelp. */
  printHelp(root);
  
  printf("\n");
}

/* Free memory used by the tree. */
void freeTree(struct TreeNode* root)
{
  
  /* Create TreeNodes to hold values. */
  /* Just as in the linked list, we are going
   * to traverse each subtree. If a subtree
   * is NULL, we will not do anything to it. */
  if(root == NULL) return;
  
  freeTree(root->left);
  freeTree(root->right);

  free(root);

}

/* A helper function to isBST. Traverses the binary tree
 * to find the minimum value. */
int minValue(struct TreeNode* root)
{
  
  /* This will follow the same logic as the method
   * that returned the maximum value, except we
   * will traverse down the left. So, */
  struct TreeNode* current = root;
  
  while(current->left != NULL)
  {
    current = current->left;
  }

  /* Return value at the last node. */
  return current->data;
}
