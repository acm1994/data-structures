#include <stdio.h>

/*********************/
/* Aaron Montano     */
/* 09/20/2016        */
/* Section 002       */
/*********************/

/* Main method. */
void main(int argc, char* argv[])
{
	
	/* First, we need to check to see if we are dealing with decimals
	* or integers in the command line. */
	if(argv[2] == "-b") printf("Binary");
	if(argv[2] == "-d") printf("Decimals!");
	
}